pragma solidity ^0.4.21;
contract Lottory {
    address public manager;
    address[] public players;
    mapping(address=>Player) playermap;
    // event logging(address p);

    struct Player{
        address playerAdd;
        uint  money;
    }

    function Lottory() public {
        manager=msg.sender;
    }

    function getPlayers() view public returns (address[]) {
        return players;
    }

    function enter() public payable enteronlyonce{
        //to do need to avoid a user repeatedly enter
        require(msg.value > 0.01 ether);
        players.push(msg.sender);
        playermap[msg.sender]=Player(msg.sender,msg.value);
    }

    function random() private view returns (uint) {
        //sha3 is also a global function
        //block is global variable that we can access any time
        //now is also global
        return uint(keccak256(block.difficulty, now, players));
    }

    function pickWinner() public restricted{
        uint index=random()%players.length;
        players[index].transfer(address(this).balance);
        // new address[](0) this way we create a brand new dynamic array, that has 0 size.
        // if we new address[](5) this way we create a brand new dynamic array, that has size 5.
        //if we want to create a fixed size array, then like this new address[5](0);
        players=new address[](0);
    }

    modifier restricted(){
        require(msg.sender==manager);
        _;
    }

    modifier enteronlyonce(){
        require(playermap[msg.sender].playerAdd == 0x0000);
        _;
    }


}

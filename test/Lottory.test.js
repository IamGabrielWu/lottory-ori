const assert = require('assert');
const ganache = require('ganache-cli');
const Web3 = require('web3');// Web3 is capitalized as it has Web3 constructor.Here Web3 is used to create instance
const web3 = new Web3(ganache.provider()); // lower case means instance
const {interface, bytecode} = require('../compile');
let accounts;
let lottory;
beforeEach(async()=>{
  // Get a list of all accounts
  accounts = await web3.eth.getAccounts()
  // Use one of those accounts to deploy contract
  // the contract
  lottory = await new web3.eth.Contract(JSON.parse(interface))
    .deploy({
      data: bytecode
    })
    .send({from: accounts[0], gas: "4700000"});
})

describe('Lottory', ()=>{
  it('deploys a contract', ()=>{
      assert.ok(lottory.options.address);
  });
  it('allows one account to enter', async ()=>{
      await lottory.methods.enter().send({
        from: accounts[0],
        value: web3.utils.toWei('0.02','ether'),
        gas: 410085
      });
      const players = await lottory.methods.getPlayers().call({
        from: accounts[0]
      });
      assert.equal(accounts[0], players[0]);
      assert.equal(1,players.length);
  });
  it('not allow one account repeatedly enter', async ()=>{
    //within every it block, contract instance is completely new. so here two times of enter. above enter within above it block is not counted.
    try{
      await lottory.methods.enter().send({
        from: accounts[0],
        value: web3.utils.toWei('0.02','ether'),
        gas: 410085
      });
      await lottory.methods.enter().send({
        from: accounts[0],
        value: web3.utils.toWei('0.02','ether'),
        gas: 410085
      });
      assert(false);
    }catch(err){
      assert(err);
    }
  });
  it('only manager can call pick winner', async ()=>{
    try{
      await lottory.methods.enter().send({
        from: accounts[0],
        value: web3.utils.toWei('0.02','ether'),
        gas: 410085
      });
      await lottory.methods.enter().send({
        from: accounts[1],
        value: web3.utils.toWei('0.02','ether'),
        gas: 410085
      });
      await lottory.methods.enter().send({
        from: accounts[2],
        value: web3.utils.toWei('0.02','ether'),
        gas: 410085
      });
      await lottory.methods.enter().send({
        from: accounts[3],
        value: web3.utils.toWei('0.02','ether'),
        gas: 410085
      });
      await lottory.methods.pickWinner().send({
        from: accounts[1],
        gas: 410085
      });
      assert(false)
    }catch(err){
      assert(err);
    }
  });
})
